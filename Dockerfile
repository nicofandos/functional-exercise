# pull the base image
FROM node:14

# set the working direction
WORKDIR /app

# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH

# Copy app dependencies
COPY package.json ./
COPY package-lock.json ./

# Install app dependencies
RUN npm install

# Add the rest of the app
COPY . ./

# Test app
CMD npm test
