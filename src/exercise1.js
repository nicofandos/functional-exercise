const fiboRec = (number, previous = 1, preprevious = 0) => {
  if (number <= 2)
    return previous + preprevious;
  return fiboRec(number - 1, previous + preprevious, previous);
}

const fibonacci = number => {
  if (number <= 0)
    return 0;
  return fiboRec(number);
};

module.exports = fibonacci;
