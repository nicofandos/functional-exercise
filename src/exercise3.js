/*
 * Returns a function that approximates the derive of fn with error h in the value given to the returned function.
 */
const fderive = (fn, h) => {
  const derived = (x) => {
    return ((fn(x + h) - fn(x-h)) / (2*h));
  };
  return derived;
}

const squear = x => {
  return Math.pow(x, 2);
}

module.exports = {
  fderive,
  squear,
};
