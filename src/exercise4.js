const wordCount = sequence => {
  let countedWords = {};
  if (sequence)
    sequence.split(' ').forEach(word => {
      let wordLower = word.toLowerCase()
      if (wordLower !== '')
        countedWords[wordLower] = countedWords[wordLower] ? countedWords[wordLower] + 1 : 1;
    });
  return Object.keys(countedWords).length > 0 ? countedWords : null;
};

module.exports = {
  wordCount,
};
