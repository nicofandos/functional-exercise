const faverage = (numbers) => {
  if (!numbers)
    return 0;
  let sum = numbers.reduce((acum, current) => {return acum + current}, 0);
  let len = Math.max(1, numbers.length);
  return sum/len;
};

module.exports = faverage;
